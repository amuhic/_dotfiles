# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
  export ZSH=/home/amuhic/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
# ZSH_THEME="robbyrussell"
ZSH_THEME="juanghurtado"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"


#

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
#############################################################
alias fucking='sudo'
alias prolog='gprolog'
alias vim='gvim -v'
alias top='htop'
alias info='info --vi-keys'

alias d1='cd /home/amuhic/Desktop/'
alias d2='cd /home/amuhic/Downloads/'
alias d3='cd /home/amuhic/Workspace/_dotfiles'
alias l1='cd /home/amuhic/Workspace/_lib/'
alias w1='cd /home/amuhic/Workspace/'
alias c1='cd /home/amuhic/Workspace/_c/'
alias c2='cd /home/amuhic/Workspace/_cpp/'
alias p1='cd /home/amuhic/Workspace/_python/'
alias pl1='cd /home/amuhic/Workspace/_prolog/'
alias p2='cd /home/amuhic/Workspace/_projects/'
alias p3='cd /home/amuhic/Workspace/_prolog/'
alias j1='cd /home/amuhic/Workspace/_java/'
alias s1='cd /home/amuhic/Workspace/_sola/'

alias ll='ls -hgl --color=always --group-directories-first'

alias java1='/usr/local/etc/jdk1.7.0_04/bin/java'
alias java2='/usr/local/etc/jdk1.7.0_04-x86/bin/java'
# alias eclimd='/home/amuhic/.eclipse/org.eclipse.platform_646169614_linux_gtk_x86_64/eclimd'
alias eclim=$ECLIPSE_HOME'eclim'
alias eclimd=$ECLIPSE_HOME'eclimd'
# alias eclim='/home/amuhic/.eclipse/org.eclipse.platform_646169614_linux_gtk_x86_64/eclim'

#Matlab libs compatibility for opencv
alias old_bash='scl enable devtoolset-1.1 bash'
alias matlabcv='LD_PRELOAD=/lib64/libgcc_s.so.1:/lib64/libGL.so.1:/lib64/libfreetype.so.6:/lib64/libstdc++.so.6:/lib64/libQt5OpenGL.so.5:/lib64/libtiff.so.5:/lib64/libQt5Core.so.5:/lib64/libQt5Gui.so.5:/lib64/libQt5Widgets.so.5 matlab'

#############################################################
MOUNTSPACE='/home/amuhic/mount/'
D1='/home/amuhic/Desktop/'
D2='/home/amuhic/Downloads/'
D3='/home/amuhic/Workspace/_dotfiles'
L1='/home/amuhic/Workspace/_lib/'
W1='/home/amuhic/Workspace/'
JAVA1='/usr/local/etc/jdk1.7.0_04/'
JAVA2='/usr/local/etc/jdk1.7.0_04-x86/'
C1='/home/amuhic/Workspace/_c/'
C2='/home/amuhic/Workspace/_cpp/'
P1='/home/amuhic/Workspace/_python/'
PL1='/home/amuhic/Workspace/_prolog/'
P2='/home/amuhic/Workspace/_projects/'
P3='/home/amuhic/Workspace/_prolog/'
J1='/home/amuhic/Workspace/_java/'
S1='/home/amuhic/Workspace/_sola/'
W='/home/amuhic/Workspace/'

# LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib:/usr/lib64:/lib:/lib64
# LD_LIBRARY_PATH=/lib:/lib64:/usr/lib:/usr/lib64:/opt/matlab/sys/opengl/lib/glnxa64:/opt/matlab/sys/os/glnxa64:/opt/matlab/bin/glnxa64:/opt/matlab/extern/lib/glnxa64:/opt/matlab/runtime/glnxa64:/opt/matlab/sys/java/jre/glnxa64/jre/lib/amd64/native_threads:/opt/matlab/sys/java/jre/glnxa64/jre/lib/amd64/server:/usr/local/lib
# export LD_LIBRARY_PATH

# ECLIPSE_HOME='/home/amuhic/.eclipse/org.eclipse.platform_646169614_linux_gtk_x86_64/'
#############################################################
PATH=$PATH:/home/amuhic/bin/
PATH=$PATH:/usr/lib64/qt4/bin/:usr/lib/qt4/bin/
export JAVA_HOME="/usr/java/jdk1.8.0_20/"

export PATH
export PYTHONSTARTUP="/home/amuhic/.pyrc"


#powerline in bash
# if [ -f ~/.local/lib/python2.7/site-packages/powerline/bindings/bash/powerline.sh ]; then
    # source ~/.local/lib/python2.7/site-packages/powerline/bindings/bash/powerline.sh
# fi
