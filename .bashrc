#default ...
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace
 
# append to the history file, don't overwrite it
shopt -s histappend
 
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

#so as not to be disturbed by Ctrl-S ctrl-Q in terminals:
stty -ixon


# LD_LIBRARY_PATH=/lib:/lib64:/usr/lib:/usr/lib64:/opt/matlab/sys/opengl/lib/glnxa64:/opt/matlab/sys/os/glnxa64:/opt/matlab/bin/glnxa64:/opt/matlab/extern/lib/glnxa64:/opt/matlab/runtime/glnxa64:/opt/matlab/sys/java/jre/glnxa64/jre/lib/amd64/native_threads:/opt/matlab/sys/java/jre/glnxa64/jre/lib/amd64/server
# export LD_LIBRARY_PATH




#############################################################
alias fucking='sudo'
alias temparature='watch -d -n 1 sensors'
alias temparature_gpu='watch -d -n 1 nvidia-smi'
alias cpu_load='watch -d -n 1 sensors'
alias gpu_load='watch -d -n 1 nvidia-smi'
alias prolog='gprolog'
#alias vim='gvim -v'
alias top='htop'
alias info='info --vi-keys'
alias ytmp3='youtube-dl --extract-audio --ignore-errors --rate-limit 600K --yes-playlist --audio-quality 0 --audio-format mp3 -o "%(title)s.%(ext)s" ' 

alias d1='cd /home/amuhic/Desktop/'
alias d2='cd /home/amuhic/Downloads/'
alias l1='cd /home/amuhic/Workspace/_lib/'
alias w1='cd /home/amuhic/Workspace/'
alias c1='cd /home/amuhic/Workspace/_c/'
alias c2='cd /home/amuhic/Workspace/_cpp/'
alias p1='cd /home/amuhic/Workspace/_python/'
alias pl1='cd /home/amuhic/Workspace/_prolog/'
alias p2='cd /home/amuhic/Workspace/Packages/'
alias j1='cd /home/amuhic/Workspace/_java/'
alias s1='cd /home/amuhic/Workspace/_sola/'

alias ll='ls -hgl --color=always --group-directories-first'

alias java1='/usr/local/etc/jdk1.7.0_04/bin/java'
alias java2='/usr/local/etc/jdk1.7.0_04-x86/bin/java'
# alias eclimd='/home/amuhic/.eclipse/org.eclipse.platform_646169614_linux_gtk_x86_64/eclimd'
alias eclim=$ECLIPSE_HOME'eclim'
alias eclimd=$ECLIPSE_HOME'eclimd'
# alias eclim='/home/amuhic/.eclipse/org.eclipse.platform_646169614_linux_gtk_x86_64/eclim'

#############################################################
MOUNTSPACE='/home/amuhic/mount/'
D1='/home/amuhic/Desktop/'
D2='/home/amuhic/Downloads/'
L1='/home/amuhic/Workspace/_lib/'
W1='/home/amuhic/Workspace/'
JAVA1='/usr/local/etc/jdk1.7.0_04/'
JAVA2='/usr/local/etc/jdk1.7.0_04-x86/'
C1='/home/amuhic/Workspace/_c/'
C2='/home/amuhic/Workspace/_cpp/'
P1='/home/amuhic/Workspace/_python/'
PL1='/home/amuhic/Workspace/_prolog/'
P2='/home/amuhic/Workspace/Packages/'
J1='/home/amuhic/Workspace/_java/'
S1='/home/amuhic/Workspace/_sola/'
W='/home/amuhic/Workspace/'

# ECLIPSE_HOME='/home/amuhic/.eclipse/org.eclipse.platform_646169614_linux_gtk_x86_64/'
#############################################################
PS1='\[\e[0;33m\][\u@\h: \w ]\$ \[\e[0m\]'
PATH=$PATH:/home/amuhic/bin/
PATH=$PATH:/usr/lib64/qt4/bin/:usr/lib/qt4/bin/
# export JAVA_HOME='/usr/java/jdk1.8.0_20/'


export PATH
export PYTHONSTARTUP="/home/amuhic/.pyrc"


#powerline in bash
# if [ -f ~/.local/lib/python2.7/site-packages/powerline/bindings/bash/powerline.sh ]; then
    # source ~/.local/lib/python2.7/site-packages/powerline/bindings/bash/powerline.sh
# fi

#cuda / tensorflow
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/cuda/lib64"
export CUDA_HOME=/usr/local/cuda

#android development
export ANDROID_SDK=~/Android/Sdk
export ANDROID_SDK_ROOT=~/Android/Sdk
export ANDROID_NDK_ROOT=~/Android/Sdk/ndk-bundle
export ANDROID_NDK=~/Android/Sdk/ndk-bundle
export ANDROID_OPENCV=~/Android/opencv3.1/
export ANDROID_ABI=armeabi-v7a
export PATH=$PATH:$ANDROID_SDK_ROOT/platform-tools
export PATH=$PATH:$ANDROID_SDK_ROOT/tools
export PATH=$PATH:$ANDROID_NDK
