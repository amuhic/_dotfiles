scriptencoding utf-8
set encoding=utf-8

"-------------------------------------------------------------------------
"VAM
"-------------------------------------------------------------------------
" put this line first in ~/.vimrc
        set nocompatible | filetype indent plugin on | syn on

        fun! EnsureVamIsOnDisk(plugin_root_dir)
          " windows users may want to use http://mawercer.de/~marc/vam/index.php
          " to fetch VAM, VAM-known-repositories and the listed plugins
          " without having to install curl, 7-zip and git tools first
          " -> BUG [4] (git-less installation)
          let vam_autoload_dir = a:plugin_root_dir.'/vim-addon-manager/autoload'
          if isdirectory(vam_autoload_dir)
            return 1
          else
            if 1 == confirm("Clone VAM into ".a:plugin_root_dir."?","&Y\n&N")
              " I'm sorry having to add this reminder. Eventually it'll pay off.
              call confirm("Remind yourself that most plugins ship with ".
                          \"documentation (README*, doc/*.txt). It is your ".
                          \"first source of knowledge. If you can't find ".
                          \"the info you're looking for in reasonable ".
                          \"time ask maintainers to improve documentation")
              call mkdir(a:plugin_root_dir, 'p')
              execute '!git clone --depth=1 git://github.com/MarcWeber/vim-addon-manager '.
                          \       shellescape(a:plugin_root_dir, 1).'/vim-addon-manager'
              " VAM runs helptags automatically when you install or update 
              " plugins
              exec 'helptags '.fnameescape(a:plugin_root_dir.'/vim-addon-manager/doc')
            endif
            return isdirectory(vam_autoload_dir)
          endif
        endfun

        fun! SetupVAM()
          " Set advanced options like this:
          " let g:vim_addon_manager = {}
          " let g:vim_addon_manager.key = value
          "     Pipe all output into a buffer which gets written to disk
          " let g:vim_addon_manager.log_to_buf =1

          " Example: drop git sources unless git is in PATH. Same plugins can
          " be installed from www.vim.org. Lookup MergeSources to get more control
          " let g:vim_addon_manager.drop_git_sources = !executable('git')
          " let g:vim_addon_manager.debug_activation = 1

          " VAM install location:
          let plugin_root_dir = expand('$HOME/.vim/vim-addons')
          if !EnsureVamIsOnDisk(plugin_root_dir)
            echohl ErrorMsg | echomsg "No VAM found!" | echohl NONE
            return
          endif
          let &rtp.=(empty(&rtp)?'':',').plugin_root_dir.'/vim-addon-manager'

"-------------------------------------------------------------------------
"LIST OF PLUGINS
" - (<c-x><c-p> complete plugin names):
"-------------------------------------------------------------------------
call vam#ActivateAddons(['The_NERD_tree', 'EasyPeasy','L9','Gundo', 'UltiSnips','tComment','Tagbar', 'ack', 'powerline', 'gdbvim', 'jellybeans', 'gruvbox', 'surround','vim-latex-live-preview','vimtex','vim-snippets','YouCompleteMe','EasyGrep', 'FuzzyFinder', 'vim-startify', 'fugitive' ], {'auto_install' : 0})
"-------------------------------------------------------------------------

" TODO: startify to gitmodule!
" TODO: remove buffminiexp


" Tell VAM which plugins to fetch & load:
" sample: call vam#ActivateAddons(['pluginA','pluginB', ...], {'auto_install' : 0})

" Addons are put into plugin_root_dir/plugin-name directory
" unless those directories exist. Then they are activated.
" Activating means adding addon dirs to rtp and do some additional
" magic

" How to find addon names?
" - look up source from pool
" - (<c-x><c-p> complete plugin names):
" You can use name rewritings to point to sources:
"    ..ActivateAddons(["github:foo", .. => github://foo/vim-addon-foo
"    ..ActivateAddons(["github:user/repo", .. => github://user/repo
" Also see section "2.2. names of addons and addon sources" in VAM's documentation
        endfun
        call SetupVAM()
" experimental [E1]: load plugins lazily depending on filetype, See
" NOTES
" experimental [E2]: run after gui has been started (gvim) [3]
" option1:  au VimEnter * call SetupVAM()
" option2:  au GUIEnter * call SetupVAM()
" See BUGS sections below [*]
" Vim 7.0 users see BUGS section [3]
filetype off
" set nomore
" VAMUpdateActivated
"-------------------------------------------------------------------------


" ---------------------------------------------------------------------------
" operational settings
" ---------------------------------------------------------------------------
syntax on
set nocompatible				" vim, not vi
set ruler						" show the line number on the bar
set more						" use more prompt
set autoread					" watch for file changes
set number						" line numbers
set nohidden					" close the buffer when I close a tab (I use tabs more than buffers)
set noautowrite					" don't automagically write on :next
set lazyredraw					" don't redraw when don't have to
set mousehide					" hide the mouse when typing
set showmode					" show the mode all the time
set showcmd						" Show us the command we're typing
set noautoindent		 		" auto indent
set nocindent					" auto C indent
set nosmartindent				" more inteligent auto-indenting
"set t_Co=256					" tell terminal to use 256 color scheme
set smarttab					" tab and backspace are smart
set tabstop=4					" 4 spaces
set expandtab					" convert tabs into spaces
set shiftwidth=4				" shift width
set scrolloff=9 				" keep at least 3 lines above/below
set sidescrolloff=7				" keep at least 5 lines left/right
set backspace=indent,eol,start	" backspace over all kinds of things
set showfulltag					" show full completion tags
set linebreak					" wrap at 'breakat' instead of last char
set nowrap
set tw=120						" default textwidth is a max of 
set autochdir
set cmdheight=1					" command line height
set updatecount=100				" flush file to disk every 100 chars
set complete=.,w,b,u,U,t,i,d	" do lots of scanning on tab completion
set ttyfast						" we have a fast terminal
set backupdir=~/.vim/tmp        " save backup dir
set swapfile                    " TEST FOR SWAP
set dir=~/.vim/tmp
set laststatus=2				" always show statusbar
set wildignore+=*.0,*.obj,*.pyc,*.DS_STORE,*.db,*.swc " don't match object files
set wildmode=full				" *wild* mode
set wildignore+=*.o,*~,.lo		" ignore object files
set wildmenu					" menu has tab completion
set foldcolumn=2				" 3 lines of column for fold showing, always
set magic						" Enable the "magic"
set cursorline					" show the cursor line
set matchpairs+=<:>				" add < and > to match pairs
set matchpairs+=":"				" add < and > to match pairs
set whichwrap+=~,[,],<,>		" these wrap to
set fileencoding=utf-8
set encoding=utf-8

"error bell
set novb                        " no visual bell -.
set eb vb t_vb=
set noerrorbells				" no error bells please
set novisualbell				" Disable ALL bells

set splitbelow
set splitright

if &term =~ '256color'
  " disable Background Color Erase (BCE) so that color schemes
  " render properly when inside 256-color tmux and GNU screen.
  " see also http://snk.tuxfamily.org/log/vim-256color-bce.html
  set t_ut=
endif


set sessionoptions=blank,curdir,folds,help,tabpages,winpos

let NERDTreeHijackNetrw = 0

set viminfo='200,f1

autocmd BufEnter *.c,*.cpp,*.sh,*.py,*.m,*.M,*.tex let &cc="80,".join(range(120,999),",")
autocmd BufEnter *.java set cc="100,".join(range(120,999),",")
autocmd BufEnter *.rc set cc="100,".join(range(120,999),",")
" ---------------------------------------------------------------------------
" GUI FONT
" ---------------------------------------------------------------------------
if has("gui_running")
  " if has("gui_gtk2")
    " set guifont=Inconsolata\ for\ Powerline\ Bold\ 11
  " endif
" if has("gui_win32")
  "set guifont=Consolas:h11:cANSI
  set guifont=Droid\ Sans\ Mono\ for\ Powerline\ 12
endif
" endif
" ---------------------------------------------------------------------------


" ---------------------------------------------------------------------------
" ---------------------------------------------------------------------------
filetype on						" Enable filetype detection
filetype indent on				" Enable filetype-specific indenting
filetype plugin on				" Enable filetype-specific plugins

let maplocalleader=','			" all my shortcuts start with ,
let mapleader=','				" this leader is used by EasyTags, which is mapped to '\' by default

" to keep fold history
au BufWinLeave * silent! mkview	" save buffer view on exit
au BufWinEnter * silent! loadview " restore buffer view on enter
" ---------------------------------------------------------------------------


" ---------------------------------------------------------------------------
" searching
" ---------------------------------------------------------------------------
set incsearch					" incremental search
set ignorecase					" search ignoring case
set smartcase					" Ignore case when searching lowercase
set hlsearch					" highlight the search
set showmatch					" show matching bracket
set diffopt=filler,iwhite		" ignore all whitespace and sync
" ---------------------------------------------------------------------------

" ---------------------------------------------------------------------------
" UtilSnips 
" ---------------------------------------------------------------------------
let g:UltiSnipsExpandTrigger="<c-s>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
" ---------------------------------------------------------------------------

" ---------------------------------------------------------------------------
" youcompleteme
" ycm
" ---------------------------------------------------------------------------
let g:syntastic_auto_jump = 2
let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"
 
" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
" let g:SuperTabDefaultCompletionType = '<C-n>'

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

let g:ycm_enable_diagnostic_signs = 0

" nmap <LocalLeader>d :YcmCompleter GetDoc<CR>
" nnoremap <leader>g :YcmCompleter GoTo<CR>
nnoremap <F9> :YcmCompleter GoToDefinition<CR>
nnoremap <S-F9> :YcmCompleter GoToDeclaration<CR>
nmap <F5> <ESC>:YcmCompleter GetDoc<CR>
imap <F5> <ESC>:YcmCompleter GetDoc<CR>

let g:ycm_python_binary_path = '/usr/bin/python'
let g:ycm_server_python_interpreter = '/usr/bin/python'

set previewheight=1
set completeopt+=preview
let g:ycm_autoclose_preview_window_after_insertion = 0
let g:ycm_autoclose_preview_window_after_completion = 0
" ---------------------------------------------------------------------------


" ---------------------------------------------------------------------------
"  startify
"  start fy 
" ---------------------------------------------------------------------------
let g:startify_session_dir = '~/.vim/session'
let g:startify_session_autoload = 1
let g:startify_relative_path = 0
let g:startify_session_before_save = [
        \ 'echo "Cleaning up before saving.."',
        \ 'silent! NERDTreeTabsClose'
\ ]
let g:startify_bookmarks = [ {'c': '~/.vimrc'}, '~/.zshrc' ]
let g:startify_session_persistence = 1
let g:startify_change_to_dir = 0
" ---------------------------------------------------------------------------

" ---------------------------------------------------------------------------
" setup NERDTree
" ---------------------------------------------------------------------------
let NERDTreeWinSize=35
" autocmd VimEnter * NERDTree		" start NERD on launch
" autocmd VimEnter * wincmd p		" start with cursor in main window

" Quit VIM when NERDTree is the only window left
autocmd WinEnter * call s:CloseIfOnlyNerdTreeLeft()

" Close all open buffers on entering a window if the only
" buffer that's left is the NERDTree buffer
function! s:CloseIfOnlyNerdTreeLeft()
  if exists("t:NERDTreeBufName")
    if bufwinnr(t:NERDTreeBufName) != -1
      if winnr("$") == 1
        q
      endif
    endif
  endif
endfunction

"nerdtree toggle
autocmd VimEnter * nmap <F3> :NERDTreeToggle<CR>
autocmd VimEnter * imap <F3> <Esc>:NERDTreeToggle<CR>a
" ---------------------------------------------------------------------------


" nmap <F4> :TlistToggle<CR>
" imap <F4> <Esc>:TlistToggle<CR>a

" ---------------------------------------------------------------------------
" -- TComment --
" ---------------------------------------------------------------------------
nmap <F2> :TComment<CR>
imap <F2> <Esc>:TComment<CR>a
vmap <F2> :TComment<CR>

" ---------------------------------------------------------------------------
" -- ctags --
" ---------------------------------------------------------------------------
let g:tag_directory = "~/.vim/tags/"    
let g:tag_extension = ".tags"
let g:lvimrc_filename = ".lvimrc"       

if has("win32") || has("win16")
    let g:include_paths = "C:\MinGW\\include\\,C:\\MinGW\\lib\\gcc\\mingw32\\4.6.2\\include\\"
else
    let g:include_paths = "/usr/include/,/usr/local/include/"
endif

"map <ctrl>+f12 to generate ctags for current folder:
map <C-F12> :!ctags -f .tags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR><CR>
"map the :EasyPeasy command to CTRL-F11
nmap <C-F11> :EasyPeasy<CR>
"add current directory's generated tags file to available tags
set tags+=.tags
" And also source local vimrc files, which I use for adding extra tag files from ~/.vim/tags folder
if (filereadable(getcwd()."\\.lvimrc"))
    exec "source .lvimrc"
endif
" ---------------------------------------------------------------------------


" ---------------------------------------------------------------------------
" setup Tlist taglist
let Tlist_Use_Right_Window=1
let Tlist_Auto_Open=0
let Tlist_Enable_Fold_Column=0
let Tlist_Show_One_File = 0		" Only show tags for current buffer
let Tlist_Compact_Format=0
let Tlist_WinWidth=30
let Tlist_Exit_OnlyWindow=1
let Tlist_File_Fold_Auto_Close = 1
let Tlist_Inc_Winwidth=0

" ---------------------------------------------------------------------------
" setup FuzzyFinder
" ---------------------------------------------------------------------------
" find in buffer == ,b
nmap <LocalLeader>b :FufBuffer<CR>
" find file == ,F
" nmap <LocalLeader>FF :FufFile<CR>

nmap <LocalLeader>F :FufCoverageFile<CR>
" find in tag == ,T
nmap <LocalLeader>T :FufTag<CR>
" ---------------------------------------------------------------------------

" ---------------------------------------------------------------------------
" config for easytags
" ---------------------------------------------------------------------------
let g:easytags_file = '~/.vim/vtags'
" tag-related keybinds:
" open tag in new tab
map <c-\> :tab split<cr>:exec("tag ".expand("<cword>"))<cr>
" open tag in split with ,\
map <localleader>\ :split <cr>:exec("tag ".expand("<cword>"))<cr>
" open tag in vsplit with ,]
map <localleader>] :vsplit <cr>:exec("tag ".expand("<cword>"))<cr>

" ---------------------------------------------------------------------------
"  Grep configuration
" ---------------------------------------------------------------------------
let Grep_Skip_Files='.* *.o'	" exclude hidden and object files
" ---------------------------------------------------------------------------


" ---------------------------------------------------------------------------
"  Gundo configuration
" ---------------------------------------------------------------------------
map <LocalLeader>u :GundoToggle<CR>
let g:pyflakes_use_quickfix=0

let g:SuperTabDefaultCompletionType = "context"
" ---------------------------------------------------------------------------


" ---------------------------------------------------------------------------
"  MiniBufExpl configuration
" ---------------------------------------------------------------------------
let g:miniBufExplCheckDupeBufs = 0	" to fix performance issues with multi-tab
" ---------------------------------------------------------------------------


" ---------------------------------------------------------------------------
"  EasyGrep setup
" ---------------------------------------------------------------------------
" set recursive search
let g:EasyGrepRecursive=1
" show the results in a proper window at the bottom
let g:EasyGrepWindow=1
" smart file choose - don't search binary and config files=2
let g:EasyGrepMode=0
" match the case
let g:EasyGrepIgnoreCase=0
" when doing search & replace, do it in new tabs
let g:EasyGrepReplaceWindowMode=0
" use external grep command defined by /grepprg
let g:EasyGrepCommand=1
" ---------------------------------------------------------------------------


" ---------------------------------------------------------------------------
" Mouse stuff
" ---------------------------------------------------------------------------
" this makes the mouse paste a block of text without formatting it
" (good for code)
map <MouseMiddle> <esc>"*p
" ---------------------------------------------------------------------------

" ---------------------------------------------------------------------------
" spelling...
" ---------------------------------------------------------------------------
if v:version >= 700
setlocal spell spelllang=en
" ,ss == toggle spelling
nmap <LocalLeader>ss :set spell!<CR>
endif
" default to no spelling
set nospell
" ---------------------------------------------------------------------------

" ---------------------------------------------------------------------------
" some useful mappings
" hotkeys
" shortcuts
" ---------------------------------------------------------------------------

" QUICKER SEARCH AND REPLACE %s:::g
" noremap ;; :%s:::g<Left><Left><Left>
" noremap ;' :%s:::cg<Left><Left><Left><Left>
noremap <LocalLeader>sr :s::g<Left><Left>
noremap <LocalLeader>sg :%s::g<Left><Left>
noremap <LocalLeader>sG :%s::cg<Left><Left>
noremap <LocalLeader>sc :s::cg<Left><Left>

vnoremap ss y<ESC>/<c-r>"<CR>   

if executable('ag')
    " set grepprg=ag\ --nogroup\ --column\ --smart-case\ --nocolor\ --follow
    set grepprg=ag\ --nocolor\ --nogroup\ --column
    " set grepformat=%f:%l:%C:%m
endif 

" when navigating treat wrapped lines as separate
nnoremap j gj
nnoremap k gk
inoremap <S-Tab> <Esc> <Tab>
" Omnicomplete as Ctrl+Space
inoremap <C-Space> <C-x><C-o>
" Also map user-defined omnicompletion as Ctrl+k
inoremap <C-Space> <C-x><C-u>
" Y yanks from cursor to $
map Y y$
" map space in normal mode
noremap <space> :
" ,f == Grep search
nnoremap <LocalLeader>f :Grep 
nnoremap <LocalLeader>g :Grep 
" ,T == show TODO (already mapped to <Leader>TT)
" map <LocalLeader>T :TaskList<CR>
" jj in insert mode == Esc
"imap jj <Esc>
" toggle list mode
nmap <LocalLeader>tl :set list!<cr>
" toggle paste mode
nmap <LocalLeader>pp :set paste!<cr>
" toggle wrapping
nmap <LocalLeader>ww :set wrap!<cr>
" change directory to that of current file
nmap <LocalLeader>cd :cd%:p:h<cr>
" change local directory to that of current file
nmap <LocalLeader>lcd :lcd%:p:h<cr>
" correct type-o's on exit
nmap q: :q
" save and build
nmap <LocalLeader>wm :w<cr>:make<cr>
" open all folds
nmap <LocalLeader>o :%foldopen!<cr>
" close all folds
nmap <LocalLeader>c :%foldclose!<cr>

"Tagbar instead of taglist
nnoremap <silent> <leader>tt :TagbarToggle<cr>
" " ,tt will toggle taglist on and off
" nmap <LocalLeader>tt :Tlist<cr>
" ,n will toggle NERDTree on and off
"nmap <LocalLeader>n :NERDTreeToggle<cr>
" When I'm pretty sure that the first suggestion is correct
map <LocalLeader>st 1z=
" If I forgot to sudo vim a file, do that with :w!!
cmap w!! %!sudo tee > /dev/null %
" Fix the # at the start of the line
inoremap # X<BS>#
" Fold with paren begin/end matching
nmap <LocalLeader>z zf%
" Fold all functions (C type)
nmap <LocalLeader>Z :g/^{$/normal zf%<CR>
" When I use ,sf - return to syntax folding with a big foldcolumn
nmap <LocalLeader>sf :set foldcolumn=6 foldmethod=syntax<cr>
" GPG helpers
nmap <LocalLeader>E :1,$!gpg --armor --encrypt 2>/dev/null<CR>
nmap <LocalLeader>ES :1,$!gpg --armor --encrypt --sign 2>/dev/null<CR>
nmap <LocalLeader>S :1,$!gpg --clearsign 2>/dev/null<CR>
" Switch tabs with ctrl-tab and ctrl-shift-tab like most browsers
map <LocalLeader><Tab> gt
map <LocalLeader><S> gT 
" Switch to a specific tab
map <LocalLeader>1 1gt
map <LocalLeader>2 2gt
map <LocalLeader>3 3gt
map <LocalLeader>4 4gt
map <LocalLeader>5 5gt
map <LocalLeader>6 6gt
map <LocalLeader>7 7gt
map <LocalLeader>8 8gt
map <LocalLeader>9 9gt
map <LocalLeader>0 :tablast<CR>
" latex overrides c-j -> <C-(te)X>j
" imap <C-x>j <Plug>IMAP_JumpForward
nnoremap <C-x>j <Plug>IMAP_JumpForward
" move between splits faster
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h
" resize splits with + and -
nnoremap <silent> + :exe "resize " . (winheight(0) * 3/2)<CR>
nnoremap <silent> - :exe "resize " . (winheight(0) * 2/3)<CR>

cmap ;\ \(\)<Left><Left>

"

autocmd Filetype pl set filetype=prolog

" ---------------------------------------------------------------------------
" tabs
" (LocalLeader is ",")
" create a new tab
map <LocalLeader>tc :tabnew %<cr>
" close a tab
map <LocalLeader>td :tabclose<cr>
" next tab
map <LocalLeader>n :tabnext<cr>
map <LocalLeader>N :tabprev<cr>
" next tab
map <silent><m-Right> :tabnext<cr>
" previous tab
map <LocalLeader>tp :tabprev<cr>
" previous tab
map <silent><m-Left> :tabprev<cr>
" move a tab to a new location
map <LocalLeader>tm :tabmove

" ---------------------------------------------------------------------------
" Diff with saved version of the file
function! s:DiffWithSaved()
  let filetype=&ft
  diffthis
  vnew | r # | normal! 1Gdd
  diffthis
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! DiffSaved call s:DiffWithSaved()

" ---------------------------------------------------------------------------
" auto load extensions for different file types
if has('autocmd')
      filetype plugin indent on
      syntax on

" jump to last line edited in a given file (based on .viminfo)
    autocmd BufReadPost *
                \ if line("'\"") > 0|
                \ if line("'\"") <= line("$")|
                \ exe("norm '\"")|
                \ else|
                \ exe "norm $"|
                \ endif|
                \ endif

" improve legibility
    au BufRead quickfix setlocal nobuflisted wrap number

endif

" ---------------------------------------------------------------------------
" setup for the visual environment
if $TERM =~ '^xterm'
      set t_Co=256
elseif $TERM =~ '^screen-bce'
      set t_Co=256 " just guessing
elseif $TERM =~ '^rxvt'
      set t_Co=88
elseif $TERM =~ '^linux'
      set t_Co=8
else
      set t_Co=16
endif

" ---------------------------------------------------------------------------
" Abbreviations
ab sob /* Signed-off-by: Andrej Muhic <andre.muhic@gmail.com> */

syntax enable
if has("gui_running")
    set novb
    " colorscheme eclipse 
    " set background=light
    " set background=dark
    " colorscheme solarized
    " colorscheme jellybeans
    colorscheme gruvbox
    " colorscheme twilight
else
    set t_Co=256
    set background=dark
    "set background=light
    "colorscheme jellybeans
    colorscheme gruvbox
    " colorscheme twilight256
endif

" ---------------------------------------------------------------------------
"  Set Lua support
" ---------------------------------------------------------------------------
let lua_complete_omni=1
" ---------------------------------------------------------------------------


" ---------------------------------------------------------------------------
"  taglist
" ---------------------------------------------------------------------------
" let Tlist_Compact_Format = 1
" let Tlist_Display_Prototype = 1 "pokaze cel prototip ne rabm vec od kar ycm to dela
" let Tlist_Display_Tag_Scope = 0
" ---------------------------------------------------------------------------

" ---------------------------------------------------------------------------
" LATEX
" ---------------------------------------------------------------------------
" latex stuff
let g:tex_flavor='latex'
" indentation for tex files
au FileType tex set sw=2
"Spell check
" au FileType tex setlocal spell spelllang=en_gb
au FileType tex setlocal spell spelllang=~/.vim/spell/sl.utf-8.spl
au FileType tex setlocal tw=119
" Vim-latex rules:
" to enable \ll to run automatically for pdfs
let g:Tex_DefaultTargetFormat='pdf'
let g:Tex_MultipleCompileFormats='pdf'
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
" let g:vimtex_quickfix_mode=0
set conceallevel=1
" let g:tex_conceal="abdmg" 
let g:tex_conceal = ""
let g:livepreview_previewer = 'evince'

autocmd FileType tex imap ss š
autocmd FileType tex imap SS Š
autocmd FileType tex imap zz ž
autocmd FileType tex imap ZZ Ž
autocmd FileType tex imap cc č
autocmd FileType tex imap CC Č

if !exists('g:ycm_semantic_triggers')
	let g:ycm_semantic_triggers = {}
endif
let g:ycm_semantic_triggers.tex = g:vimtex#re#youcompleteme
" ---------------------------------------------------------------------------
